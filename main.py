import requests
from bs4 import BeautifulSoup as BS
import csv


def get_html(url):
    """Получаем HTML код страницы"""
    r = requests.get(url)
    return r.text


def refined(rating):
    """Форматируем число рейтингов в подходящий формат"""
    rating = rating.split(' ')[0]
    rating = rating.replace(',', '')
    return rating


def write_csv(data):
    """Записываем данные в CSV"""
    with open('plugins.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow((
            data['name'],
            data['url'],
            data['rating']
        ))


def get_data(html):
    soup = BS(html, 'lxml')
    popular = soup.find_all('section', class_='plugin-section')[3]
    plugins = popular.find_all('article')

    for plugin in plugins:
        name = plugin.find('h3', class_='entry-title').text
        url = plugin.find('h3', class_='entry-title').find('a').get('href')
        rating = plugin.find('div', class_='plugin-rating').find('span', class_='rating-count').find('a').text
        rating = refined(rating)

        data = {
            'name': name,
            'url': url,
            'rating': rating
        }

        write_csv(data)

    return plugins


def main():
    url = 'https://wordpress.org/plugins/'
    get_data(get_html(url))


if __name__ == '__main__':
    main()


from django.db import models
from mptt import models as mpttmodels


class Category(mpttmodels.MPTTModel):
    """Категории"""
    parent = mpttmodels.TreeForeignKey('self', on_delete=models.SET_NULL, verbose_name='Родительская категория', null=True, blank=True, related_name='childen', db_index=True)
    name = models.CharField(verbose_name='Навзвание категории', max_length=255)
    description = models.TextField(verbose_name='Описание', blank=True)
    photo = models.ImageField(verbose_name='Фото', upload_to='category_photos', blank=True)
    preview_photo = models.ImageField(verbose_name='Фото превью', upload_to='category_photos', blank=True)
    main = models.BooleanField(verbose_name='Основная категория', default=False)

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = 'name'

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Brand(models.Model):
    """Производитель"""
    name = models.CharField(verbose_name='Название', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'


class Product(models.Model):
    """Товары"""
    category = mpttmodels.TreeForeignKey('Category', verbose_name='Категория', related_name='childen', db_index=True)
    name = models.CharField(verbose_name='Товар', max_length=255)
    brand = models.ForeignKey(Brand,verbose_name='Бренд', on_delete=models.CASCADE)
    short_description = models.TextField(verbose_name='Краткое описание')
    description = models.TextField(verbose_name='Описание', blank=True)
    preview_photo = models.ImageField(verbose_name='Фото превью', upload_to='product_photos', blank=True)
    accessory = models.ManyToManyField('self', verbose_name='Аксессуар')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class ProductPhoto(models.Model):
    """Фотографии товаров"""
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    photo = models.ImageField(verbose_name='Фотография', upload_to='product_images')

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'


class ParamType(models.Model):
    """Типы характеристики"""
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название характеристики', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип характристики'
        verbose_name_plural = 'Типы характеристик'


class ParamValue(models.Model):
    """Значения характеристики"""
    param_type = models.ForeignKey(ParamType, verbose_name='Тип характеристики', on_delete=models.CASCADE)
    value = models.CharField(verbose_name='Значение', max_length=255)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = 'Значние характеристики'
        verbose_name_plural = 'Значения характеристик'


class ParamGroup(models.Model):
    """Группы типов характеристик"""
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)
    param_type = models.ManyToManyField(ParamType, verbose_name='Типы характеристик')

    class Meta:
        verbose_name = 'Группа типов характеристик'
        verbose_name_plural = 'Группы типов характеристик'


class ParamValueSet(models.Model):
    """Группы занчений характеристик"""
    param_group = models.ForeignKey(ParamGroup, verbose_name='Группа параметров', on_delete=models.CASCADE)
    param_value = models.ManyToManyField(ParamValue, verbose_name='Значения характеристик')
    popular = models.BooleanField(verbose_name='Популярные товар', default=False)
    stock = models.BooleanField(verbose_name='Акции месяца', default=False)
    new = models.BooleanField(verbose_name='Новинки', default=False)
    default = models.BooleanField(verbose_name='Главная вариация товара', default=False)

    class Meta:
        verbose_name = 'Группа значений характеристик'
        verbose_name_plural = 'Группы значений характеристик'


class Prices(models.Model):
    """Цены"""
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)
    param_value_set = models.ForeignKey(ParamValueSet, verbose_name='Группа значений характеристик', on_delete=models.CASCADE)
    retail_price = models.DecimalField(verbose_name='Розничная цена', max_digits=9, decimal_places=2)
    wholesale_price =models.DecimalField(verbose_name='Отптовая цена', max_digits=9, decimal_places=2)

    class Meta:
        verbose_name = 'Цена'
        verbose_name_plural = 'Цены'


class StorageRemains(models.Model):
    """Остатки на складе"""
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)
    param_value_set = models.ForeignKey(ParamValueSet, verbose_name='Группа значений характеристик', on_delete=models.CASCADE)
    count = models.IntegerField(verbose_name='Кол-во на складе', default=0)

    def __str__(self):
        return self.count

    class Meta:
        verbose_name = 'Остаток на складе'
        verbose_name_plural = 'Остатки на складе'


class Order(models.Model):
    """Заказы"""
    address = models.CharField(verbose_name='Адрес', max_length=255)
    total = models.IntegerField(verbose_name='Итоговая цена')
    data = models.DateTimeField(verbose_name='Дата и время', blank=True)
    email = models.EmailField(verbose_name='Email', max_length=255)
    phone = models.CharField(verbose_name='Телефон', max_length=20)
    status = models.BooleanField(verbose_name='Статус оплаты', default=False)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class OrderProduct(models.Model):
    """Товары в заказах"""
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)
    order = models.ForeignKey(Order, verbose_name='Заказ', on_delete=models.CASCADE)
    param_value_set = models.ForeignKey(ParamValueSet, verbose_name='Группа занченний характеристик', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(verbose_name='Кол-во', default=1)
    price = models.DecimalField(verbose_name='Общая цена', max_digits=9, decimal_places=2)
    discount = models.FloatField(verbose_name='Скидка', default=0)

    class Meta:
        verbose_name = 'Товар из заказа'
        verbose_name_plural = 'Товары из заказов'
